FROM maven:3.8.5-openjdk-8-slim AS build
WORKDIR /
COPY /src /src
COPY pom.xml /
RUN mvn -f /pom.xml clean package

FROM openjdk:8-jdk-slim
WORKDIR /
COPY /src /src
COPY --from=build /target/*.jar accounting_system.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "accounting_system.jar"]

LABEL authors="vladislav-semenov"
