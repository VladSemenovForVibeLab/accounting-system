package com.example.accountingsystemformanagingcontractsandreports.repository;

import com.example.accountingsystemformanagingcontractsandreports.model.ContractStage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContractStageRepository extends JpaRepository<ContractStage, Long> {
    List<ContractStage> findByContract_Id(Long contractId);
}