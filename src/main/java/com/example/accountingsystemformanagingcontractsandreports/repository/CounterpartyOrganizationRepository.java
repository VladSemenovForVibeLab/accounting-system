package com.example.accountingsystemformanagingcontractsandreports.repository;

import com.example.accountingsystemformanagingcontractsandreports.model.CounterpartyOrganization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CounterpartyOrganizationRepository extends JpaRepository<CounterpartyOrganization, Long> {
}