package com.example.accountingsystemformanagingcontractsandreports.repository;

import com.example.accountingsystemformanagingcontractsandreports.model.ContractWithCounterparty;
import com.example.accountingsystemformanagingcontractsandreports.model.enums.ContractType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContractWithCounterpartyRepository extends JpaRepository<ContractWithCounterparty, Long> {
    List<ContractWithCounterparty> findByContractType(ContractType type);

    List<ContractWithCounterparty> findByCounterparty_Id(Long counterpartyId);
}