package com.example.accountingsystemformanagingcontractsandreports.repository;

import com.example.accountingsystemformanagingcontractsandreports.model.Contract;
import com.example.accountingsystemformanagingcontractsandreports.model.enums.ContractType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ContractRepository extends JpaRepository<Contract, Long> {
    List<Contract> findAllByPlannedStartDateBetween(Date startDate, Date endDate);
    List<Contract> findByContractType(ContractType type);

    List<Contract> findByContractors_Id(Long counterpartyId);
}