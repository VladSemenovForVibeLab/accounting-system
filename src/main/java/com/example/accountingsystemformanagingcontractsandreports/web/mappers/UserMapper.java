package com.example.accountingsystemformanagingcontractsandreports.web.mappers;

import com.example.accountingsystemformanagingcontractsandreports.model.User;
import com.example.accountingsystemformanagingcontractsandreports.web.dto.UserDto;

public interface UserMapper extends Mappable<User, UserDto> {
}
