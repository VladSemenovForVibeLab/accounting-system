package com.example.accountingsystemformanagingcontractsandreports.web.controller;

import com.example.accountingsystemformanagingcontractsandreports.model.Contract;
import com.example.accountingsystemformanagingcontractsandreports.model.enums.ContractType;
import com.example.accountingsystemformanagingcontractsandreports.service.interf.CRUDService;
import com.example.accountingsystemformanagingcontractsandreports.service.interf.ContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;
import java.util.List;

@Service
@RequestMapping(ContractRestController.CONTRACT_REST_CONTROLLER_URL)
public class ContractRestController extends CRUDRestController<Contract, Long> {
    public static final String CONTRACT_REST_CONTROLLER_URL = "/api/v1/contracts";
    @Autowired
    private ContractService contractService;

    @Override
    CRUDService<Contract, Long> getService() {
        return contractService;
    }
    @GetMapping("/by-type/{type}")
    public List<Contract> getContractsByType(@PathVariable ContractType type) {
        return contractService.findContractsByType(type);
    }
    @GetMapping("/by-date-range")
    public List<Contract> getContractsByDateRange(@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
                                                  @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate) {
        return contractService.findContractsByDateRange(startDate, endDate);
    }
    @GetMapping("/by-counterparty/{counterpartyId}")
    public List<Contract> getContractsByCounterparty(@PathVariable Long counterpartyId) {
        return contractService.getContractsByCounterparty(counterpartyId);
    }
}
