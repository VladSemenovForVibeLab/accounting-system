package com.example.accountingsystemformanagingcontractsandreports.web.controller;

import com.example.accountingsystemformanagingcontractsandreports.model.User;
import com.example.accountingsystemformanagingcontractsandreports.service.interf.CRUDService;
import com.example.accountingsystemformanagingcontractsandreports.service.interf.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(UserRestController.USER_REST_URL)
public class UserRestController extends CRUDRestController<User,Long>{
    public static final String USER_REST_URL="/api/v1/users";
    @Autowired
    private UserService userService;
    @Override
    CRUDService<User, Long> getService() {
        return userService;
    }
}
