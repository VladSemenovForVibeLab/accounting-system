package com.example.accountingsystemformanagingcontractsandreports.web.controller;

import com.example.accountingsystemformanagingcontractsandreports.model.ContractStage;
import com.example.accountingsystemformanagingcontractsandreports.service.interf.CRUDService;
import com.example.accountingsystemformanagingcontractsandreports.service.interf.ContractStageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping
public class ContractStageRestController extends CRUDRestController<ContractStage,Long> {
    public static final String CONTRACT_STAGE_REST_CONTROLLER_URL="/api/v1/contractStages";
    @Autowired
    private ContractStageService contractStageService;
    @Override
    CRUDService<ContractStage, Long> getService() {
        return contractStageService;
    }
    @GetMapping("/by-contract/{contractId}")
    public List<ContractStage> getContractStagesByContract(@PathVariable Long contractId) {
        return contractStageService.getContractStageByContract(contractId);
    }
}
