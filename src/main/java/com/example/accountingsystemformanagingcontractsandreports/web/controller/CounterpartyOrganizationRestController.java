package com.example.accountingsystemformanagingcontractsandreports.web.controller;

import com.example.accountingsystemformanagingcontractsandreports.model.CounterpartyOrganization;
import com.example.accountingsystemformanagingcontractsandreports.service.interf.CRUDService;
import com.example.accountingsystemformanagingcontractsandreports.service.interf.CounterpartyOrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(CounterpartyOrganizationRestController.COUNTERPARTY_ORGANIZATION_REST_CONTROLLER)
public class CounterpartyOrganizationRestController extends CRUDRestController<CounterpartyOrganization,Long> {
    public static final String COUNTERPARTY_ORGANIZATION_REST_CONTROLLER="/api/v1/counterpartyOrganizations";
    @Autowired
    private CounterpartyOrganizationService counterpartyOrganizationService;
    @Override
    CRUDService<CounterpartyOrganization, Long> getService() {
        return counterpartyOrganizationService;
    }
}
