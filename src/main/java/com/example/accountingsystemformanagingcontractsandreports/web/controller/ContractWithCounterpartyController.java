package com.example.accountingsystemformanagingcontractsandreports.web.controller;

import com.example.accountingsystemformanagingcontractsandreports.model.ContractWithCounterparty;
import com.example.accountingsystemformanagingcontractsandreports.model.enums.ContractType;
import com.example.accountingsystemformanagingcontractsandreports.service.interf.CRUDService;
import com.example.accountingsystemformanagingcontractsandreports.service.interf.ContractWithCounterpartyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(ContractWithCounterpartyController.CONTRACT_WITH_COUNTERPARTY_CONTROLLER_URL)
public class ContractWithCounterpartyController extends CRUDRestController<ContractWithCounterparty,Long> {
    public static final String CONTRACT_WITH_COUNTERPARTY_CONTROLLER_URL="/api/v1/contractsWithCounterparties";

    @Autowired
    private ContractWithCounterpartyService contractWithCounterpartyService;
    @Override
    CRUDService<ContractWithCounterparty, Long> getService() {
        return contractWithCounterpartyService;
    }
    @GetMapping("/by-type/{type}")
    public List<ContractWithCounterparty> getContractsByType(@PathVariable ContractType type) {
        return contractWithCounterpartyService.findContractsByType(type);
    }

    @GetMapping("/by-counterparty/{counterpartyId}")
    public List<ContractWithCounterparty> getContractsByCounterparty(@PathVariable Long counterpartyId) {
        return contractWithCounterpartyService.findContractsByCounterparty(counterpartyId);
    }
}
