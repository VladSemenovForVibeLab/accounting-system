package com.example.accountingsystemformanagingcontractsandreports.web.controller;

import com.example.accountingsystemformanagingcontractsandreports.model.User;
import com.example.accountingsystemformanagingcontractsandreports.service.interf.AuthService;
import com.example.accountingsystemformanagingcontractsandreports.service.interf.UserService;
import com.example.accountingsystemformanagingcontractsandreports.web.dto.UserDto;
import com.example.accountingsystemformanagingcontractsandreports.web.dto.auth.JWTRequest;
import com.example.accountingsystemformanagingcontractsandreports.web.dto.auth.JWTResponse;
import com.example.accountingsystemformanagingcontractsandreports.web.dto.validation.OnCreate;
import com.example.accountingsystemformanagingcontractsandreports.web.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(AuthenticationRestController.AUTHENTICATION_REST_CONTROLLER_URL)
public class AuthenticationRestController {
    public static final String AUTHENTICATION_REST_CONTROLLER_URL="/api/v1/authentications";
    @Autowired
    private AuthService authService;
    @Autowired
    private UserService userService;
    @PostMapping("/login")
    public JWTResponse login(@Validated @RequestBody JWTRequest loginRequest){
        return authService.login(loginRequest);
    }
    @PostMapping("/register")
    public void register(@Validated(OnCreate.class) @RequestBody User user){
        userService.create(user);
    }
    @PostMapping("/refresh")
    public JWTResponse refresh(@RequestBody String refreshToken){
        return authService.refresh(refreshToken);
    }
}
