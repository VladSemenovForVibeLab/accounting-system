package com.example.accountingsystemformanagingcontractsandreports.web.dto.auth;

import lombok.Data;

@Data
public class JWTResponse {
    private Long id;
    private String username;
    private String accessToken;
    private String refreshToken;
}

