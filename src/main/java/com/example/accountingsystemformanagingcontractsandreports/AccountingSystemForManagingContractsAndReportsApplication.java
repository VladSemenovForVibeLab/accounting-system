package com.example.accountingsystemformanagingcontractsandreports;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccountingSystemForManagingContractsAndReportsApplication {

    public static void main(String[] args) {
        SpringApplication.run(AccountingSystemForManagingContractsAndReportsApplication.class, args);
    }

}
