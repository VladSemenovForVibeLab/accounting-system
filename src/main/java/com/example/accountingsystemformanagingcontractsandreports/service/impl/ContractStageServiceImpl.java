package com.example.accountingsystemformanagingcontractsandreports.service.impl;

import com.example.accountingsystemformanagingcontractsandreports.model.ContractStage;
import com.example.accountingsystemformanagingcontractsandreports.repository.ContractStageRepository;
import com.example.accountingsystemformanagingcontractsandreports.service.interf.ContractStageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.swing.text.html.CSS;
import java.util.List;

@Service
public class ContractStageServiceImpl extends AbstractCRUDService<ContractStage,Long> implements ContractStageService {
    @Autowired
    private ContractStageRepository contractStageRepository;
    @Override
    JpaRepository<ContractStage, Long> getRepository() {
        return contractStageRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ContractStage> getContractStageByContract(Long contractId) {
        return contractStageRepository.findByContract_Id(contractId);
    }
}
