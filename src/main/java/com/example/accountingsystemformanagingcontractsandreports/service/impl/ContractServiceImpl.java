package com.example.accountingsystemformanagingcontractsandreports.service.impl;

import com.example.accountingsystemformanagingcontractsandreports.model.Contract;
import com.example.accountingsystemformanagingcontractsandreports.model.enums.ContractType;
import com.example.accountingsystemformanagingcontractsandreports.repository.ContractRepository;
import com.example.accountingsystemformanagingcontractsandreports.service.interf.ContractService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ContractServiceImpl extends AbstractCRUDService<Contract,Long> implements ContractService {
    @Autowired
    private ContractRepository contractRepository;
    @Override
    JpaRepository<Contract, Long> getRepository() {
        return contractRepository;
    }

    @Override
    public List<Contract> findContractsByType(ContractType type) {
        return contractRepository.findByContractType(type);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Contract> findContractsByDateRange(Date startDate, Date endDate) {
        return contractRepository.findAllByPlannedStartDateBetween(startDate, endDate);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Contract> getContractsByCounterparty(Long counterpartyId) {
        return contractRepository.findByContractors_Id(counterpartyId);
    }
}
