package com.example.accountingsystemformanagingcontractsandreports.service.impl;

import com.example.accountingsystemformanagingcontractsandreports.model.CounterpartyOrganization;
import com.example.accountingsystemformanagingcontractsandreports.repository.CounterpartyOrganizationRepository;
import com.example.accountingsystemformanagingcontractsandreports.service.interf.CounterpartyOrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class CounterpartyOrganizationServiceImpl extends AbstractCRUDService<CounterpartyOrganization,Long> implements CounterpartyOrganizationService {
    @Autowired
    private CounterpartyOrganizationRepository counterpartyOrganizationRepository;
    @Override
    JpaRepository<CounterpartyOrganization, Long> getRepository() {
        return counterpartyOrganizationRepository;
    }
}
