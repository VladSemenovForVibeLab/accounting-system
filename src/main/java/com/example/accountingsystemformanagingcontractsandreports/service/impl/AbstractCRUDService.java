package com.example.accountingsystemformanagingcontractsandreports.service.impl;

import com.example.accountingsystemformanagingcontractsandreports.service.interf.CRUDService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractCRUDService<E,K> implements CRUDService<E,K> {
    abstract JpaRepository<E,K> getRepository();

    @Override
    @Transactional
    public void create(E entity) {
        getRepository().save(entity);
    }

    @Override
    @Transactional(readOnly = true)
    public E findById(K id) {
        return getRepository().findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<E> findAll() {
        List<E> objects = new ArrayList<>();
        getRepository().findAll().forEach(objects::add);
        return objects;

    }

    @Override
    @Transactional
    public E update(E entity) {
        getRepository().save(entity);
        return entity;
    }
    @Override
    @Transactional
    public void delete(E entity) {
        getRepository().delete(entity);
    }
}
