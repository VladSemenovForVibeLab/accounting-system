package com.example.accountingsystemformanagingcontractsandreports.service.impl;

import com.example.accountingsystemformanagingcontractsandreports.model.User;
import com.example.accountingsystemformanagingcontractsandreports.model.enums.Role;
import com.example.accountingsystemformanagingcontractsandreports.model.exception.ResourceNotFoundException;
import com.example.accountingsystemformanagingcontractsandreports.repository.UserRepository;
import com.example.accountingsystemformanagingcontractsandreports.service.interf.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserServiceImpl extends AbstractCRUDService<User,Long> implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Override
    JpaRepository<User, Long> getRepository() {
        return userRepository;
    }

    @Override
    @Transactional
    public User update(User entity) {
        entity.setPassword(passwordEncoder.encode(entity.getPassword()));
        userRepository.save(entity);
        return entity;
    }

    @Override
    @Transactional
    public void create(User entity) {
        if(userRepository.findByUsername(entity.getUsername()).isPresent()){
            throw new IllegalStateException("User already exists.");
        }
        if(!entity.getPassword().equals(entity.getPasswordConfirmation())){
            throw new IllegalStateException("Password and password confirmation do not match");
        }
        entity.setPassword(passwordEncoder.encode(entity.getPassword()));
        Set<Role> roles = new HashSet<>();
        roles.add(Role.ROLE_USER);
        entity.setRoles(roles);
        userRepository.save(entity);
    }

    @Override
    @Transactional(readOnly = true)
    public User getByUsername(String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(()->new ResourceNotFoundException("User not found"));
    }
}
