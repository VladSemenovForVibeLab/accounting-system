package com.example.accountingsystemformanagingcontractsandreports.service.impl;

import com.example.accountingsystemformanagingcontractsandreports.model.ContractWithCounterparty;
import com.example.accountingsystemformanagingcontractsandreports.model.enums.ContractType;
import com.example.accountingsystemformanagingcontractsandreports.repository.ContractWithCounterpartyRepository;
import com.example.accountingsystemformanagingcontractsandreports.service.interf.ContractWithCounterpartyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ContractWithCounterpartyServiceImpl extends AbstractCRUDService<ContractWithCounterparty,Long> implements ContractWithCounterpartyService {
    @Autowired
    private ContractWithCounterpartyRepository contractWithCounterpartyRepository;
    @Override
    JpaRepository<ContractWithCounterparty, Long> getRepository() {
        return contractWithCounterpartyRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ContractWithCounterparty> findContractsByType(ContractType type) {
        return contractWithCounterpartyRepository.findByContractType(type);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ContractWithCounterparty> findContractsByCounterparty(Long counterpartyId) {
        return contractWithCounterpartyRepository.findByCounterparty_Id(counterpartyId);
    }
}
