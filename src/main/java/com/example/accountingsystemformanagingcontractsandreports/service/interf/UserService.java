package com.example.accountingsystemformanagingcontractsandreports.service.interf;

import com.example.accountingsystemformanagingcontractsandreports.model.User;

public interface UserService extends CRUDService<User,Long> {
    User getByUsername(String username);
}
