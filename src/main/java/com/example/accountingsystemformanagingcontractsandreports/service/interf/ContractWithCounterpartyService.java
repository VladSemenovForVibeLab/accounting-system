package com.example.accountingsystemformanagingcontractsandreports.service.interf;

import com.example.accountingsystemformanagingcontractsandreports.model.ContractWithCounterparty;
import com.example.accountingsystemformanagingcontractsandreports.model.enums.ContractType;

import java.util.List;

public interface ContractWithCounterpartyService extends CRUDService<ContractWithCounterparty,Long> {
    List<ContractWithCounterparty> findContractsByType(ContractType type);
    List<ContractWithCounterparty> findContractsByCounterparty(Long counterpartyId);
}
