package com.example.accountingsystemformanagingcontractsandreports.service.interf;

import com.example.accountingsystemformanagingcontractsandreports.model.CounterpartyOrganization;

public interface CounterpartyOrganizationService extends CRUDService<CounterpartyOrganization,Long> {

}
