package com.example.accountingsystemformanagingcontractsandreports.service.interf;

import com.example.accountingsystemformanagingcontractsandreports.model.ContractStage;

import java.util.List;

public interface ContractStageService extends CRUDService<ContractStage,Long> {
    List<ContractStage> getContractStageByContract(Long contractId);
}
