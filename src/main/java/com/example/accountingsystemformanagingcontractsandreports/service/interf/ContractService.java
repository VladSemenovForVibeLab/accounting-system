package com.example.accountingsystemformanagingcontractsandreports.service.interf;

import com.example.accountingsystemformanagingcontractsandreports.model.Contract;
import com.example.accountingsystemformanagingcontractsandreports.model.enums.ContractType;

import java.util.Date;
import java.util.List;

public interface ContractService extends CRUDService<Contract,Long> {
    List<Contract> findContractsByType(ContractType type);
    List<Contract> findContractsByDateRange(Date startDate,Date endDate);

    List<Contract> getContractsByCounterparty(Long counterpartyId);
}
