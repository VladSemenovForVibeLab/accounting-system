package com.example.accountingsystemformanagingcontractsandreports.service.interf;

import com.example.accountingsystemformanagingcontractsandreports.web.dto.auth.JWTRequest;
import com.example.accountingsystemformanagingcontractsandreports.web.dto.auth.JWTResponse;

public interface AuthService {
    JWTResponse login(JWTRequest loginRequest);

    JWTResponse refresh(String refreshToken);

}
