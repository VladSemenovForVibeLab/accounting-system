package com.example.accountingsystemformanagingcontractsandreports.model.exception;

public class ResourceNotFoundException  extends RuntimeException{
    public ResourceNotFoundException(String message) {
        super(message);
    }
}

