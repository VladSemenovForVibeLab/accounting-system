package com.example.accountingsystemformanagingcontractsandreports.model.enums;

public enum Role {
    ROLE_USER,
    ROLE_ADMIN

}
