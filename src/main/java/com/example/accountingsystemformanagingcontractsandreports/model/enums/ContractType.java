package com.example.accountingsystemformanagingcontractsandreports.model.enums;

public enum ContractType {
    PURCHASE,
    SUPPLY,
    WORKS
}
