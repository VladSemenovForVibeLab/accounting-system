package com.example.accountingsystemformanagingcontractsandreports.model;

import com.example.accountingsystemformanagingcontractsandreports.model.enums.ContractType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "contract")
public class Contract {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contract_id", nullable = false)
    private Long id;
    @Column(name = "name")
    private String name;
    @Enumerated(EnumType.STRING)
    private ContractType contractType;
    @Temporal(TemporalType.DATE)
    private Date plannedStartDate;
    @Temporal(TemporalType.DATE)
    private Date plannedEndDate;
    @Temporal(TemporalType.DATE)
    private Date actualStartDate;
    @Temporal(TemporalType.DATE)
    private Date actualEndDate;
    @Column(name = "contract_amount")
    private double contractAmount;
    @OneToMany(mappedBy = "contract")
    private List<ContractStage> stages;
    @ManyToMany
    @JoinTable(
            name = "contract_contractor",
            joinColumns = @JoinColumn(name = "contract_id"),
            inverseJoinColumns = @JoinColumn(name = "contractor_id")
    )
    private List<ContractWithCounterparty> contractors;
}