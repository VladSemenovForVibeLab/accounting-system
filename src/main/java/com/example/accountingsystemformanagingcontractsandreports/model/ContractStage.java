package com.example.accountingsystemformanagingcontractsandreports.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "contract_stage")
public class ContractStage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "name")
    private String name;
    @Temporal(TemporalType.DATE)
    private Date plannedStartDate;
    @Temporal(TemporalType.DATE)
    private Date plannedEndDate;
    @Temporal(TemporalType.DATE)
    private Date actualStartDate;
    @Temporal(TemporalType.DATE)
    private Date actualEndDate;
    @Column(name = "stage_amount")
    private double stageAmount;
    @ManyToOne
    @JoinColumn(name = "contract_id")
    private Contract contract;
    @Column(name = "material_costs")
    private double materialCosts;
    @Column(name = "salary_costs")
    private double salaryCosts;
}