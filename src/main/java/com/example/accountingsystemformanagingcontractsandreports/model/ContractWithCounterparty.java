package com.example.accountingsystemformanagingcontractsandreports.model;

import com.example.accountingsystemformanagingcontractsandreports.model.enums.ContractType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "contract_with_counterparty")
public class ContractWithCounterparty {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "name")
    private String name;
    @Enumerated(EnumType.STRING)
    private ContractType contractType;
    @ManyToOne
    @JoinColumn(name = "counterparty_id")
    private CounterpartyOrganization counterparty;
    @Column(name = "contract_amount")
    private double contractAmount;
    @Temporal(TemporalType.DATE)
    private Date plannedStartDate;
    @Temporal(TemporalType.DATE)
    private Date plannedEndDate;
    @Temporal(TemporalType.DATE)
    private Date actualStartDate;
    @Temporal(TemporalType.DATE)
    private Date actualEndDate;
    @Column(name = "material_costs")
    private double materialCosts;
    @Column(name = "salary_costs")
    private double salaryCosts;
}